<?php

namespace src\Bitm\SEIP_108252\Gender;

class Gender {
    
    
    public $id ="id";
    public $gender ="gender";
    public $created ="created";
    public $modified ="modified";
    public $created_by ="create_by";
    public $modified_by ="modified_by";
    public $deleted_at ="deleted_at";
    
    
    public function __construct($gender=FALSE) {
        echo "gender"."</br>"; 
    }
    
    public function index(){
        echo " I am listing data";
    }
    
    
    public function create(){
        echo " I am creating form";
    }
    
    
    public function store(){
        echo " I am storing data";
    }
    
    
    public function edit(){
        echo " I am editing form";
    }
    
    
    public function update(){
        echo " I am updating data";
    }
    
    
    public function delete(){
        echo " I delete data";
    }
   

}
