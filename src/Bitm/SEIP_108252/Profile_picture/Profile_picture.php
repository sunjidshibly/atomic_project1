<?php

namespace src\Bitm\SEIP_108252\Profile_picture;

class Profile_picture {
    public $id ="id";
    public $profile_picture ="profile_picture";
    public $created ="created";
    public $modified ="modified";
    public $created_by ="create_by";
    public $modified_by ="modified_by";
    public $deleted_at ="deleted_at";
    
    
    public function __construct($profile_picture=FALSE) {
        echo "profile_picture"."</br>"; 
    }
    
    public function index(){
        echo " I am listing data";
    }
    
    
    public function create(){
        echo " I am creating form";
    }
    
    
    public function store(){
        echo " I am storing data";
    }
    
    
    public function edit(){
        echo " I am editing form";
    }
    
    
    public function update(){
        echo " I am updating data";
    }
    
    
    public function delete(){
        echo " I delete data";
    }
   
}
