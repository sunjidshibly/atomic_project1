<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Atomic project 1</title>
    <link rel="stylesheet" type="text/css" href="stylesheet.css">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

  </head>
  <body>
    <div class="wrapper">
        <div class="header">
          </br>
          <h1>Welcome to my project</h1>
          <h2>
       Name: Shibly Mohammad </br> SEIP:108252
          </h2>   
        </div>
        <div class="content">         
            <a href="view/SEIP_108252/Book_title/"> 
            <div class="card-container" >
                <div class="card">
                    <div class="side"> </br><br/><img src="src/img/icon3.png" alt="Icon" style="width:45px;height:45px;"><h3>Book List</h3></div>
                    <div class="side back"></br><br/><img src="src/img/icon3.png" alt="Icon" style="width:45px;height:45px;"><h3>Shows the list of books and their's details</h3></div>
                </div>
            </div></a>

            <a href="view/SEIP_108252/Birthday/"> 
            <div class="card-container" >
                <div class="card">
                    <div class="side"> </br><br/><img src="src/img/icon4.png" alt="Icon" style="width:45px;height:45px;"><h3>Birthday</h3></div>
                    <div class="side back"></br><br/><img src="src/img/icon4.png" alt="Icon" style="width:45px;height:45px;"><h3>Shows the list of user's birthday</h3></div>
                </div>
            </div></a>

            <a href="view/SEIP_108252/Email/"> 
            <div class="card-container" >
                <div class="card">
                    <div class="side"> </br><br/><img src="src/img/icon2.png" alt="Icon" style="width:45px;height:45px;"><h3>Email</h3></div>
                    <div class="side back"></br><br/><img src="src/img/icon2.png" alt="Icon" style="width:45px;height:45px;"><h3>Shows the list of user's email</h3></div>
                </div>
            </div></a>

            <a href="view/SEIP_108252/Hobby/"> 
            <div class="card-container" >
                <div class="card">
                    <div class="side"> </br><br/><img src="src/img/icon6.png" alt="Icon" style="width:45px;height:45px;"><h3>Hobby</h3></div>
                    <div class="side back"></br><br/><img src="src/img/icon6.png" alt="Icon" style="width:45px;height:45px;"><h3>Shows the list of User's hobby</h3></div>
                </div>
            </div></a>

            <a href="view/SEIP_108252/City/"> 
            <div class="card-container" >
                <div class="card">
                    <div class="side"> </br><br/><img src="src/img/icon6.png" alt="Icon" style="width:45px;height:45px;"><h3>City</h3></div>
                    <div class="side back"></br><br/><img src="src/img/icon6.png" alt="Icon" style="width:45px;height:45px;"><h3>Shows the list of Cities</h3></div>
                </div>
            </div></a>

            <a href="view/SEIP_108252/Organization/"> 
            <div class="card-container" >
                <div class="card">
                    <div class="side"> </br><br/><img src="src/img/icon3.png" alt="Icon" style="width:45px;height:45px;"><h3>Org. Summary</h3></div>
                    <div class="side back"></br><br/><img src="src/img/icon3.png" alt="Icon" style="width:45px;height:45px;"><h3>Shows the Summary of a Organization</h3></div>
                </div>
            </div></a>

            <a href="view/SEIP_108252/Gender/"> 
            <div class="card-container" >
                <div class="card">
                    <div class="side"> </br><br/><img src="src/img/icon2.png" alt="Icon" style="width:45px;height:45px;"><h3>Gender</h3></div>
                    <div class="side back"></br><br/><img src="src/img/icon2.png" alt="Icon" style="width:45px;height:45px;"><h3>Shows the user's gender</h3></div>
                </div>
            </div></a>

            <a href="view/SEIP_108252/Profile_picture/"> 
            <div class="card-container" >
                <div class="card">
                    <div class="side"> </br><br/><img src="src/img/icon5.png" alt="Icon" style="width:45px;height:45px;"><h3>Profile Picture</h3></div>
                    <div class="side back"></br><br/><img src="src/img/icon5.png" alt="Icon" style="width:45px;height:45px;"><h3>Shows the profile picture of user</h3></div>
                </div>
            </div></a>

            <a href="view/SEIP_108252/Terms_condition/"> 
            <div class="card-container" >
                <div class="card">
                    <div class="side"> </br><br/><img src="src/img/icon4.png" alt="Icon" style="width:45px;height:45px;"><h3>Terms and Condition</h3></div>
                    <div class="side back"></br><br/><img src="src/img/icon4.png" alt="Icon" style="width:45px;height:45px;"><h3>Make user agree to the terms and condition</h3></div>
                </div>
            </div></a>

        </div>
        <div class="footer"> 
            <a href=""><img src="src/img/facebook.png" alt="facebook" /></a>
            <a href=""><img src="src/img/twitter.png" alt="twitter" /></a>
            <a href=""><img src="src/img/linkedin.png" alt="linkin" /></a>  
            </br>
             Copyright: #Suntech 2015
        </div>
    </div>
    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>